Neural Network for Voice Activity Detection
===========================================

Dataset
-------
Before running any scripts, please create an symbolic link to the dataset:
```
ln -s path-to-speech-dataset data
```
Note that the dataset folder is named `data`.

Installation and Update
-----------------------

Install:
```
git clone git@bitbucket.org:chungheng/spikingvad.git
```

Update:
```
git pull
```

Useful Commands
---------------
### tmux
Create `tmux` session,
```
tmux new-session -s <new-of-session>
```
Detach from `tmux`,
```
Ctrl+d
```
Re-attach to a `tmux` session
```
tmux attach
```
or
```
tmux attach -t <new-of-session>
```

### Conda
List all installed `conda` environments,
```
conda env list
```

Activate a `conda` environment,
```
source activate <env-name>
```

### Slurm
Running with GPU resource,
```
srun --gres=gpu:1 --pty python <name-of-script>
```

### H5
```python
import h5py

fp = h5py.File('filename.h5')

# List attributes
fp.attrs.items()

# List datasets
fp.keys()

# Get value of a dataset as a numpy array
fp['name-of-dataset'].value
```
