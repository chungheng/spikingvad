Dataset
-------
Please note that the NoisySpeech dataset should follow the below format,

    $ path-to-dataset/NoisySpeech/[training|evaluation|testing]
    
The only difference is that `training` (`evaluation` and `testing`) starts with lowercase.

Usage
-----

1. `prefix`: prefix to the dataset. The code no longer assumes the path to dataset. Please provide it, ex.,

    $ python fully_connected_feed.py --prefix ../../../data
    
2. `dataset`: specify the dataset to use. Multiple datasets are allowed, ex.,

    $ python fully_connected_feed.py --prefix ../../../data  --dataset 'c/q2' --dataset 'N/q2/bus/5'

    The above command will train a network with both Q2 CleanSpeech and 5dB bus noise of Q2 NoisySpeech.

    $ python fully_connected_feed.py --prefix ../../../data  --dataset 'c/q2,4'

    The above command will train a network with both Q2 and Q4 CleanSpeech.

    $ python fully_connected_feed.py --prefix ../../../data  --dataset 'N/q2,4/bus,station/5,0'

    The above command will train a network with 0dB and 5dB in both bus and station noise of Q2 and Q4 NoisySpeech.

    $ python fully_connected_feed.py --prefix ../../../data  --dataset 'N/q2,4/bus'

    The above command will train a network with bus noise of _all_ dB options in both Q2 and Q4 NoisySpeech.

    $ python fully_connected_feed.py --prefix ../../../data  --dataset 'N/q2,4'

    The above command will train a network with _all_ noise type of _all_ dB options in both Q2 and Q4 NoisySpeech.

    $ python fully_connected_feed.py --prefix ../../../data  --dataset 'N/q2,4'
    
    The above command will train a network with _all_ noise type of _all_ dB options in _all_ Q options in NoisySpeech.
    
    