# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Functions for reading VAD data."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gzip
import os
import tempfile

import numpy
import h5py
import binascii
from itertools import product
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

from tensorflow.contrib.learn.python.learn.datasets import base
from tensorflow.python.framework import dtypes


def load_data_x(filepath):
    with h5py.File(filepath,'r') as f:
        x = numpy.asarray(f.get(u'real')).T
    return x

def load_data_y(filepath):
    with open(filepath,'rb') as f:
        y = numpy.asarray([int(binascii.hexlify(b)) for b in f.read()])
    return y

def process_filepath(datasets, prefix=''):
    """
    parse dataset to filepath on system

    input:
    datasets: list of strings
        ex. ['CleanSpeech/Q2', 'Noisy/Q2/Bus/-5,10']

    return: list of strings
        return list of filepaths
    """
    def process_featureQ(featureQ):
        _featureQ_list = ['2','4','6']
        if featureQ == "*":
            featureQ = _featureQ_list
        else:
            featureQ = [x.strip(' ') for x in featureQ.split(',')]

        output = []
        for x in featureQ:
            if x[-1] not in _featureQ_list:
                print('Feature16Q' + x[-1] + ' not available')
            else:
                output.append('Feature16Q' + x[-1])
        return output

    def process_noise(noise):
        _noise_list = ['bus','cafe','cafeteria','car','field','metro','park',
            'restaurant','river','square','station','traffic']
        if noise == "*":
            noise = _noise_list
        else:
            noise = [x.strip(' ') for x in noise.split(',')]

        output = []
        for x in noise:
            if x not in _noise_list:
                print('Noise Type ' + x + ' not available')
            else:
                output.append(x)
        return output

    def process_db(db):
        _db_list = ['-5','0','5','10','15']
        if db == "*":
            db = _db_list
        else:
            db = [x.strip(' ') for x in db.split(',')]

        output = []
        for x in db:
            if x not in _db_list:
                print('Decibel Value ' + x + ' not available')
            else:
                output.append(x)
        return output

    def process_cleanspeech(seg):
        output = [['CleanSpeech'],None,['%stotN.h5']]
        if len(seg) == 1:
            seg.append('*')
        output[1] = process_featureQ( seg[1].strip(' ') )
        return output

    def process_noisyspeech(seg):
        output = [['NoisySpeech'],['%s'],None,[]]
        if len(seg) == 1:
            seg.append('*')
        output[2] = process_featureQ( seg[1].strip(' ') )

        # process noise types
        if len(seg) == 2:
            seg.append('*')
        noise_types = process_noise( seg[2].strip(' ') )

        # process noise types
        if len(seg) == 3:
            seg.append('*')
        db_types = process_db( seg[3].strip(' ') )
        output[3] = [n+d+'dB.h5' for n in noise_types for d in db_types]
        return output

    output = []
    for ds in datasets:
        seg = ds.split('/')
        c = seg[0][0].lower()
        if c == 'c':
            seg = process_cleanspeech(seg)
        elif c == 'n':
            seg = process_noisyspeech(seg)
        else:
            raise ValueError
        for x in product(*seg):
            abs_path = os.path.join(prefix, *x)
            output.append(abs_path)
    return output

class DataSet(object):

    def __init__(self,
                 windows,
                 labels,
                 fake_data=False,
                 one_hot=False,
                 dtype=dtypes.float32,
                 context_window=None):
        """Construct a DataSet.
        one_hot arg is used only if fake_data is true.  `dtype` can be either
        `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
        `[0, 1]`.
        """
        dtype = dtypes.as_dtype(dtype).base_dtype
        if dtype not in (dtypes.uint8, dtypes.float32):
            raise TypeError('Invalid window dtype %r, expected uint8 or float32' %
                          dtype)
        if fake_data:
            self._num_examples = 10000
            self.one_hot = one_hot
        else:
            assert windows.shape[0] == labels.shape[0], (
                'windows.shape: %s labels.shape: %s' % (windows.shape, labels.shape))
            self._num_examples = windows.shape[0]

            if dtype == dtypes.float32:
                # Convert from [0, 255] -> [0.0, 1.0].
                windows = windows.astype(numpy.float32)
                #windows = numpy.multiply(windows, 1.0 / 255.0)

        self._context_window = context_window or 0
        self._windows = windows
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0

        if self._context_window is not 0:
            if 0 not in self._context_window:
                raise ValueError("Context window should contain 0")

            cw = numpy.asarray(self._context_window)
            cw.sort()

            if (cw[0] > 0) or (cw[-1] < 0):
                raise ValueError("Invalid context window range")

            begin_idx = -cw[0]
            end_idx = self._windows.shape[0] - cw[-1]

            self._labels = self._labels[begin_idx:end_idx]
            self._windows = numpy.hstack([self._windows[(begin_idx+i):(end_idx+i),:] for i in cw])

            self._num_examples = self._windows.shape[0]

    @classmethod
    def merge(cls, datasets):
         def checkEqualIvo(lst):
             return not lst or lst.count(lst[0]) == len(lst)
         assert(checkEqualIvo([x._context_window for x in datasets]))
         assert(checkEqualIvo([x._windows.shape[1] for x in datasets]))

         windows = numpy.vstack([x._windows for x in datasets])
         labels = numpy.hstack([x._labels for x in datasets])

         newDataSet = cls(windows, labels)
         newDataSet._context_window = datasets[0]._context_window
         return newDataSet

    @property
    def windows(self):
        return self._windows

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size, fake_data=False):
        """Return the next `batch_size` examples from this data set."""
        if fake_data:
            fake_window = [1] * 16
            if self.one_hot:
                fake_label = [1] + [0] * 9
            else:
                fake_label = 0
            return [fake_window for _ in xrange(batch_size)], [
                  fake_label for _ in xrange(batch_size)
                ]
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = numpy.arange(self._num_examples)
            numpy.random.shuffle(perm)
            self._windows = self._windows[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._windows[start:end], self._labels[start:end]

def read_data_sets(data_set,
                   train_dir,
                   fake_data=False,
                   one_hot=False,
                   dtype=dtypes.float32,
                   context_window=None,
                   validation_size=5000,
                   prefix="../../../"):

    if fake_data:

        def fake():
            return DataSet([], [], fake_data=True, one_hot=one_hot, dtype=dtype)

        train = fake()
        validation = fake()
        test = fake()
        return base.Datasets(train=train, validation=validation, test=test)

    training_raw = []
    validation_raw = []
    testing_raw = []

    training_y = load_data_y(os.path.join(prefix,'CleanSpeech/Labels/trainingtot.vad'))
    validation_y = load_data_y(os.path.join(prefix,'CleanSpeech/Labels/evaluationtot.vad'))
    testing_y = load_data_y(os.path.join(prefix,'CleanSpeech/Labels/testingtot.vad'))

    filepaths = process_filepath(data_set, prefix)
    for fp in filepaths:
        # load testing dataset
        x = load_data_x(fp % 'training')
        training_raw.append(DataSet(x, training_y, dtype=dtype, context_window=context_window))

        # load validation dataset
        x = load_data_x(fp % 'evaluation')
        validation_raw.append(DataSet(x, validation_y, dtype=dtype, context_window=context_window))

        # load testing dataset
        x = load_data_x(fp % 'testing')
        testing_raw.append(DataSet(x, testing_y, dtype=dtype, context_window=context_window))

    train = DataSet.merge(training_raw)
    validation = DataSet.merge(validation_raw)
    test = DataSet.merge(testing_raw)

    return base.Datasets(train=train, validation=validation, test=test)

