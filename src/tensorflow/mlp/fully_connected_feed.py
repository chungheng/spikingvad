# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Trains and Evaluates the MLP network using a feed dictionary.

This code is based on fully_connected_feed.py of the official Tensorflow package.
"""

# pylint: disable=missing-docstring
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os.path
import time

from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

import input_data
import vad_mlp

# Basic model parameters as external flags.
flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_float('learning_rate', 0.01, 'Initial learning rate.')
flags.DEFINE_integer('max_epoch', 2000, 'Number of epochs.')
flags.DEFINE_integer('hidden1', 128, 'Number of units in hidden layer 1.')
flags.DEFINE_integer('hidden2', 32, 'Number of units in hidden layer 2.')
flags.DEFINE_integer('batch_size', 200, 'Batch size.  '
                     'Must divide evenly into the dataset sizes.')
flags.DEFINE_string('train_dir', 'data', 'Directory to put the training data.')
flags.DEFINE_boolean('fake_data', False, 'If true, uses fake data '
                     'for unit testing.')
flags.DEFINE_string('context_window', '-2, -1, 0, 1, 2', 'Context window, a string '
                    'representing a list of integers speparated by commas.')
flags.DEFINE_string('prefix', '', 'Prefix to the dataset.')

# Bypass tensorflow flags, and use the argument parse directly
global_parser = flags._global_parser

global_parser.add_argument('--dataset', default=[], help='dataset name', action='append')

def placeholder_inputs(batch_size, window_size):
  """Generate placeholder variables to represent the input tensors.

  These placeholders are used as inputs by the rest of the model building
  code and will be fed from the downloaded data in the .run() loop, below.

  Args:
    batch_size: The batch size will be baked into both placeholders.

  Returns:
    windows_placeholder: Windows placeholder.
    labels_placeholder: Labels placeholder.
  """
  # Note that the shapes of the placeholders match the shapes of the full
  # window and label tensors, except the first dimension is now batch_size
  # rather than the full size of the train or test data sets.
  windows_placeholder = tf.placeholder(tf.float32, shape=(batch_size,
                                                         16*window_size))
  labels_placeholder = tf.placeholder(tf.int32, shape=(batch_size))
  return windows_placeholder, labels_placeholder


def fill_feed_dict(data_set, windows_pl, labels_pl):
  """Fills the feed_dict for training the given step.

  A feed_dict takes the form of:
  feed_dict = {
      <placeholder>: <tensor of values to be passed for placeholder>,
      ....
  }

  Args:
    data_set: The set of windows and labels, from input_data.read_data_sets()
    windows_pl: The windows placeholder, from placeholder_inputs().
    labels_pl: The labels placeholder, from placeholder_inputs().

  Returns:
    feed_dict: The feed dictionary mapping from placeholders to values.
  """
  # Create the feed_dict for the placeholders filled with the next
  # `batch size` examples.
  windows_feed, labels_feed = data_set.next_batch(FLAGS.batch_size,
                                                 FLAGS.fake_data)
  feed_dict = {
      windows_pl: windows_feed,
      labels_pl: labels_feed,
  }
  return feed_dict


def do_eval(sess,
            eval_correct,
            eval_auc,
            windows_placeholder,
            labels_placeholder,
            data_set):
  """Runs one evaluation against the full epoch of data.

  Args:
    sess: The session in which the model has been trained.
    eval_correct: The Tensor that returns the number of correct predictions.
    windows_placeholder: The windows placeholder.
    labels_placeholder: The labels placeholder.
    data_set: The set of windows and labels to evaluate, from
      input_data.read_data_sets().
  """
  # And run one epoch of eval.
  true_count = 0  # Counts the number of correct predictions.
  steps_per_epoch = data_set.num_examples // FLAGS.batch_size
  num_examples = steps_per_epoch * FLAGS.batch_size
  auc_tf = eval_auc[0]
  auc_update_op = eval_auc[1]
  sess.run(tf.initialize_local_variables())
  for step in xrange(steps_per_epoch):
    feed_dict = fill_feed_dict(data_set,
                               windows_placeholder,
                               labels_placeholder)
    acc, _ = sess.run([eval_correct, auc_update_op], feed_dict=feed_dict)
    true_count += acc
  precision = true_count / num_examples
  auc = sess.run(auc_tf)
  print('  Num examples: %d  Num correct: %d  Precision @ 1: %0.04f AUC: %f' %
        (num_examples, true_count, precision, auc))


def run_training():
  """Train vad_mlp for a number of steps."""
  # unpack context window
  if not FLAGS.dataset:
      FLAGS.dataset.append('CleanSpeech')
  context_window = FLAGS.context_window
  if type(context_window) is str:
      context_window = [int(x) for x in context_window.split(',')]
  elif context_window is None:
      context_window = [0]
  else:
      raise TypeError("Context window should be either None or a string.")


  # Get the sets of windows and labels for training, validation, and
  # test on vad_mlp.
  data_sets = input_data.read_data_sets(FLAGS.dataset, FLAGS.train_dir,
      FLAGS.fake_data, context_window=context_window, prefix=FLAGS.prefix)

  # Tell TensorFlow that the model will be built into the default Graph.
  with tf.Graph().as_default():
    # Generate placeholders for the windows and labels.
    windows_placeholder, labels_placeholder = placeholder_inputs(
        FLAGS.batch_size, len(context_window))

    # Build a Graph that computes predictions from the inference model.
    logits = vad_mlp.inference(windows_placeholder,
                             FLAGS.hidden1,
                             FLAGS.hidden2)

    # Add to the Graph the Ops for loss calculation.
    loss = vad_mlp.loss(logits, labels_placeholder)

    # Add to the Graph the Ops that calculate and apply gradients.
    train_op = vad_mlp.training(loss, FLAGS.learning_rate)

    # Add the Op to compare the logits to the labels during evaluation.
    eval_correct = vad_mlp.evaluation(logits, labels_placeholder)

    # Add the Op to calculate AUC
    eval_auc = vad_mlp.compute_auc(logits, labels_placeholder)

    # Build the summary Tensor based on the TF collection of Summaries.
    summary = tf.merge_all_summaries()

    # Add the variable initializer Op.
    init = tf.initialize_all_variables()

    # Create a saver for writing training checkpoints.
    saver = tf.train.Saver()

    # Create a session for running Ops on the Graph.
    sess = tf.Session()

    # Instantiate a SummaryWriter to output summaries and the Graph.
    summary_writer = tf.train.SummaryWriter(FLAGS.train_dir, sess.graph)

    # And then after everything is built:

    # Run the Op to initialize the variables.
    sess.run(init)

    # Start the training loop.
    step = 1
    steps_per_epoch = data_sets.train.windows.shape[0] // FLAGS.batch_size
    start_time = time.time()
    epoch_start_time = time.time()
    for epoch in xrange(FLAGS.max_epoch):
        for i in xrange(steps_per_epoch):


            # Fill a feed dictionary with the actual set of windows and labels
            # for this particular training step.
            feed_dict = fill_feed_dict(data_sets.train,
                                       windows_placeholder,
                                       labels_placeholder)

            # Run one step of the model.  The return values are the activations
            # from the `train_op` (which is discarded) and the `loss` Op.  To
            # inspect the values of your Ops or variables, you may include them
            # in the list passed to sess.run() and the value tensors will be
            # returned in the tuple from the call.
            _, loss_value = sess.run([train_op, loss],
                                     feed_dict=feed_dict)


            # Write the summaries and print an overview fairly often.
            if step % 1000 == 0:
                duration = time.time() - start_time
                # Print status to stdout.
                print('Step %d: loss = %.2f (%.3f sec)' % (step, loss_value, duration))
                # Update the events file.
                summary_str = sess.run(summary, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, step)
                summary_writer.flush()
                start_time = time.time()

            step += 1

        # Save a checkpoint and evaluate the model periodically.
        if (epoch + 1) % 5 == 0 or (epoch + 1) == FLAGS.max_epoch:
            duration = time.time() - epoch_start_time
            epoch_start_time = time.time()
            print('End of Epoch %d: (%.3f sec)' % (epoch+1, duration))

            checkpoint_file = os.path.join(FLAGS.train_dir, 'checkpoint')
            saver.save(sess, checkpoint_file, global_step=step)
            # Evaluate against the training set.
            print('Training Data Eval:')
            do_eval(sess,
                    eval_correct,
                    eval_auc,
                    windows_placeholder,
                    labels_placeholder,
                    data_sets.train)
            # Evaluate against the validation set.
            print('Validation Data Eval:')
            do_eval(sess,
                    eval_correct,
                    eval_auc,
                    windows_placeholder,
                    labels_placeholder,
                    data_sets.validation)
            # Evaluate against the test set.
            print('Test Data Eval:')
            do_eval(sess,
                    eval_correct,
                    eval_auc,
                    windows_placeholder,
                    labels_placeholder,
                    data_sets.test)


def main(_):
  run_training()


if __name__ == '__main__':
  tf.app.run()
