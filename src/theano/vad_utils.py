"""
Utility functions for voice activity detection

This code is based on
[1] http://deeplearning.net/tutorial/logreg.html
"""


from collections import OrderedDict
from itertools import product

import binascii
import gzip
import h5py
import numpy
import os
import pickle
import random
import scipy.io
import stat
import subprocess
import sys

import theano
import theano.tensor as T

def shared_dataset(data_xy, borrow=True):
    """ Function that loads the dataset into shared variables

    The reason we store our dataset in shared variables is to allow
    Theano to copy it into the GPU memory (when code is run on GPU).
    Since copying data into the GPU is slow, copying a minibatch everytime
    is needed (the default behaviour if the data is not in a shared
    variable) would lead to a large decrease in performance.
    """
    data_x, data_y = data_xy
    shared_x = theano.shared(numpy.asarray(data_x,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    shared_y = theano.shared(numpy.asarray(data_y,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    # When storing data on the GPU it has to be stored as floats
    # therefore we will store the labels as ``floatX`` as well
    # (``shared_y`` does exactly that). But during our computations
    # we need them as ints (we use labels as index, and if they are
    # floats it doesn't make sense) therefore instead of returning
    # ``shared_y`` we will have to cast it to int. This little hack
    # lets ous get around this issue
    return shared_x, T.cast(shared_y, 'int32')

def load_cleanspeech_data(dataset):
    '''
    Load the dataset

    '''

    def get_filepath(dataset):
        abs_path = os.path.join(
            "..",
            "data",
            "CleanSpeech",
            dataset
        )
        return abs_path

    x_filepath = get_filepath('Feature16/%stotN.h5' % dataset)
    y_filepath = get_filepath('Labels/%stot.vad' % dataset)
    with h5py.File(x_filepath,'r') as f:
        x = numpy.asarray(f.get(u'real')).T
    with open(y_filepath,'rb') as f:
        y = numpy.asarray([int(binascii.hexlify(b)) for b in f.read()])
    return (x,y)

def grid_search(func, gridfunc=None, verbose=False, **kwargs):
    """
    Generic function for grid search

    :type func: callable
    :param func: function to perform grid search upon.

    :type gridfunc: callable
    :param gridfunc: a function for setting keyword's value based on the
    current parameters. Use this function to synthesize model name, ex.
    "mlp_lr0.001_nhidden100", or to set result directory, ex.
    "./result/test/mlp_hidden100".

    :type verbose: boolean
    :param verbose: to print out grid summaryy or not to

    :type kwargs: dict
    :param kwargs: dictionay of parameters for func. Keys should be valid input
    arguments of func, and their corresponding value could be a scalar or a list
    or values that would iterated over, ex, {'n_epochs:10, n_hidden:[100,150]'}

    Examples:

    >>> def test_func(a=1,b=2,c=3, prod=False):
            if prod is True:
                print("test_func: a*b*c=%d" % (a*b*c,))
            else:
                print("test_func: a=%d, b=%d, c=%d" % (a,b,c))

    >>> grid_search(test_func, verbose=True, **{'a':[1,2,3],'b':1})

    Use wrapper function to set input arguments:

    >>> def test_func_wrap(**kwargs):
            return test_func(b=1, prod=True, **kwargs)
    >>> grid_search(test_func_wrap, verbose=True, **{'a':[1,2,3]})

    Use gridfunc to change data directory

    >>> def gfunc(args):
            return {'dirname':'mlp_'+str(args['n_hidden'])}

    >>> grid_search(test_mlp, gridfunc=gfunc, **{'n_hidden':[100,200,300]})

    """

    # create parameters grid
    makelist = lambda x: x if hasattr(x, '__iter__') else (x,)
    kwargs = OrderedDict({k:makelist(v) for k,v in kwargs.items()})
    grid = product(*kwargs.values())
    n_grid = numpy.prod(map(len,(kwargs.values())))

    print('... starting grid search with %d combinations' % n_grid)
    for i, params in enumerate(grid):
        args = {k:v for k,v in zip(kwargs.keys(),params)}
        if gridfunc is not None:
            args.update(gridfunc(args))
        # print out parameters
        if verbose:
            print('=== Parameter set: %.4d/%.4d, %2.2f%% ===' %
                  (i+1,n_grid,100.*float(i+1)/float(n_grid)))
            for k,v in args.items():
                print('[%s]: %s' % (str(k),str(v)))
            print('... running function %s' % func.__name__)
        func(**args)
    print('... end of grid search\n')

