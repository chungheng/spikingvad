
import numpy as np
import time
from collections import OrderedDict

def init_val(val):
    def init_val_decorator(func):
        func.val = val
        def func_wrapper(x):
            res = func(x, func.val)
            if res:
                func.val = x
            return res
        return func_wrapper
    return init_val_decorator

class Monitor(object):
    def __init__(self, epochs=None, datasets=None, funcs=None, h5fp=None):


        self.epoch = 0
        self.index = 0

        if hasattr(epochs, '__len__'):
            self.num_epochs = len(epochs)
            self._epoch_msg = "Epoch %d took %f s"
            self.epochs = epochs

        else:
            self.num_epochs = epochs
            self._epoch_msg = "Epoch %d of " + str(self.num_epochs) + " took %f s"
            self.epochs = range(1, 1+self.num_epochs)
        self.epoch_gen = (x for x in self.epochs)


        self.funcs = funcs
        self.datasets = {k:v['dataset'] for k,v in datasets.items() if v}

        self.config, self.records = self.get_config(datasets)

        self.h5fp = self.setup_h5fp(h5fp)



    def get_config(self, datasets):
        config = OrderedDict({})
        records = OrderedDict({})

        for k,v  in datasets.items():
            records[k] = OrderedDict({})
            if v['dataset'] is None:
                records[k]['metrics'] = {x:0. for x in v['metrics']}
                continue

            config[k] = OrderedDict({})


            funcs = getattr(v, "eval", self.funcs)
            if not hasattr(funcs, "__len__"):
                funcs = [funcs]
            config[k]["eval"] = funcs

            metrics = sum([f() for f in funcs], [])
            assert( len(metrics) == len(set(metrics)) )
            config[k]["metrics"] = metrics
            records[k]['metrics'] = {x:0. for x in metrics}

            assert( np.all([x[0] in metrics for x in v["criterions"]]) )
            criterions = OrderedDict({})
            for x in v["criterions"]:
                f = self._get_optimal_func(x[1])
                n = []
                if x[2] == "alone":
                    n.append(k)
                elif x[2] == "all":
                    n.extend([p for p,q in datasets.items() if q['dataset']])
                elif hasattr(x[2], "__len__"):
                    n.extend(x[2])
                assert(np.all( y in datasets.keys() for y in n ))
                criterions[x[0]] =  (f, n)
            config[k]["criterions"] = criterions

        for k,v in config.items():
            if not v:
                continue
            records[k]["best"] = OrderedDict({})
            for p,q in v["criterions"].items():
                records[k]["best"][p] = {'epoch':0, 'value':0., 'datasets':{}}
                for y in q[1]:
                    records[k]["best"][p]["datasets"][y] = records[y]["metrics"].copy()

        return config, records


    def setup_h5fp(self, h5fp):
        if h5fp is  None:
            return h5fp

        h5fp.attrs["epoch duration"] = 0.
        h5fp.attrs["epochs"] = self.epochs
        for k,v in self.records.items():
            grp = h5fp.create_group(k)
            for m in v["metrics"].keys():
                grp.create_dataset(m, (self.num_epochs,), dtype='float')

            if "best" not in v.keys():
                continue

            for b in v["best"].keys():
                grp.attrs["best "+b+" value"] = 0.
                grp.attrs["best "+b+" epoch"] = 0.
        return h5fp

    def _get_optimal_func(self, f):
        if callable(f):
            return f
        elif type(f) == str:
            if f == "largest" or f == "biggest":
                return init_val(-np.inf)(lambda x,y: x>y)
            elif f == "lowest" or f == "smallest":
                return init_val(np.inf)(lambda x,y: x<y)
        else:
            raise TypeError()

    def update(self, epoch_duration=0., **kwargs):
        start_time = time.time()
        self.epoch = self.epoch_gen.next()
        self.index += 1

        for k,v in self.config.items():
            if not v:
                continue
            ds = self.datasets[k]
            for f in v["eval"]:
                r = f(*ds)
                self.records[k]["metrics"].update(r)

        for k,v in self.config.items():
            if not v:
                continue
            for c,(f,ds) in v["criterions"].items():
                if f(self.records[k]["metrics"][c]):
                    self.records[k]["best"][c]["epoch"] = self.epoch
                    self.records[k]["best"][c]["value"] = self.records[k]["metrics"][c]
                    for x in ds:
                        self.records[k]["best"][c]["datasets"][x].update(self.records[x]['metrics'])

        for k,v in kwargs.items():
            self.records[k]["metrics"].update(v)

        epoch_duration += time.time() - start_time
        self._update_console(epoch_duration)
        self._update_h5fp(epoch_duration)


    def _update_console(self, epoch_duration):
        print(self._epoch_msg % (self.epoch, epoch_duration))

        for k,v in self.records.items():
            for m,r in v['metrics'].items():
                labels = "  [{0}] {1}:".format(k,m).ljust(30)
                print "{0}{1:f}".format(labels, r)
        print "  ".ljust(38, "-")

        for k,v in self.records.items():
            if "best" not in v.keys():
                continue
            for m,r in v["best"].items():
                labels = "  [{0}] best [{1}]:".format(k,m).ljust(30)
                print "{0}{1:f}".format(labels, r["value"])
                print "    epoch:".ljust(30) + "{0:d}".format(r["epoch"])
                for d,s in r['datasets'].items():
                    for p,q in s.items():
                        labels = "    {0} {1}:".format(d,p).ljust(30)
                        print "{0}{1:f}".format(labels, q)

    def _update_h5fp(self, epoch_duration):
        if not self.h5fp:
            return
        self.h5fp.attrs["epoch duration"] = epoch_duration
        for d,v in self.records.items():
            for m,r in v["metrics"].items():
                self.h5fp[d][m][self.index-1] = r
            if "best" not in v.keys():
                continue
            for m,r in v["best"].items():
                self.h5fp[d].attrs["best "+m+" value"] = r["value"]
                self.h5fp[d].attrs["best "+m+" epoch"] = r["epoch"]

if __name__ == "__main__":

    import numpy as np


    valDataY = np.random.randint(1, size=(100,))
    valDataX = np.random.rand(valDataY.size, 5)

    test1DataY = np.random.randint(1, size=(10,))
    test1DataX = np.random.rand(valDataY.size, 5)

    test2DataY = np.random.randint(1, size=(10,))
    test2DataX = np.random.rand(valDataY.size, 5)


    def eval(x=None, y=None):
        if x is None and y is None:
            return ['auc','loss','err']
        return {
            'auc': np.random.rand(),
            'loss': np.random.rand(),
            'err': np.random.rand()
        }


    monitor = Monitor(
        num_epochs = 100,
        datasets = OrderedDict({
            'validation': {
                'dataset': (valDataX, valDataY),
                "criterions": [
                    ('auc', 'largest', 'all'),
                    ('err', 'lowest',  'all'),
                ],
                "eval": [ eval ]
            },
            'test1': {
                'dataset': (test1DataX, test1DataY),
                "criterions": [
                    ('auc', 'largest', 'alone'),
                    ('err', 'lowest', 'alone')
                ],
                "eval": [ eval ]
            },
        }),
        funcs = [eval]
    )
    for i in xrange(5):
        monitor.update()
