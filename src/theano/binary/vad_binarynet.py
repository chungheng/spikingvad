
from __future__ import print_function

import argparse
import sys
import os
import time
import h5py

import numpy as np
np.random.seed(1234)  # for reproducibility

# specifying the gpu to use
# import theano.sandbox.cuda
# theano.sandbox.cuda.use('gpu1')
import theano
import theano.tensor as T

import lasagne

import cPickle as pickle
import gzip


from collections import OrderedDict

import binary_net
import batch_norm
import shift_batch_norm
import input_data

parser = argparse.ArgumentParser()

parser.add_argument('--hinge', default=True, type=bool,
                    help='initial learning rate.')
parser.add_argument('--onehot', default=True, type=bool,
                    help='Whether to.')
parser.add_argument('--learning_rate', default=0.0003, type=float,
                    help='initial learning rate.')
parser.add_argument('--dropout_in', default=0.0, type=float,
                    help='Drop out rate of input data.')
parser.add_argument('--dropout_hidden', default=0.0, type=float,
                    help='Drop out rate of hidden layer.')
parser.add_argument('--training_scale', default=-1., type=float,
                    help='Scaling factor of training datasets.')
parser.add_argument('--testing_scale', default=-1., type=float,
                    help='Scaling factor of testing datasets.')
parser.add_argument('--num_epochs', default=2000, type=int,
                    help='Number of epochs.')
parser.add_argument('--hidden_layers', default=[60,24,11], type=int, nargs='*',
                    help='A list of number of units in each hidden layers, ex. 128, 32.')
parser.add_argument('--batch_size', default=200, type=int,
                    help='Batch size.  Must divide evenly into the dataset sizes.')
parser.add_argument('--context_window', default=[-3, 0, 3], type=int, nargs='*',
                    help='Context window, a string representing a list of '
                         'integers speparated by commas.')
parser.add_argument('--prefix', default='', type=str,
                    help='Prefix to the dataset.')
parser.add_argument('--dirpath', default='', type=str,
                    help='directory to save parameter models and training progress. ' +
                         'Default is the time that the program is executed, ex. ' + '[1-Jan-1990-12:00:00]')
parser.add_argument('--parampath', default='', type=str,
                    help='Directory name to save model parameters across epochs.')
parser.add_argument('--recordpath', default='', type=str,
                    help='File name to save training progress.')
parser.add_argument('--note', default='', type=str, help='Note.')
parser.add_argument('--shift_batch_norm', action="store_true",
                    dest="shift_batch_norm", default=False,
                    help="Turn off batch normalization.")
parser.add_argument('--no_batch_norm', action="store_false", dest="batch_norm",
                    default=True, help="Turn off batch normalization.")
parser.add_argument('--zero_bias', action="store_true", dest="zero_bias",
                    default=False, help="Force bias in DenseLayer to zero.")
parser.add_argument('--dataset', default=[], help='dataset name.', action='append')
parser.add_argument('--testing_dataset', default=[], type=str, nargs='*',
                    help='testing dataset version.')
parser.add_argument('--leakage_spike', default=[], type=int, nargs='*',
                    help="Noisy leakage spike generator in the following format, [min, max, repeat]")
parser.add_argument('--balance_classes', action="store_true", dest="balance_classes",
                    default=False, help="Balance 0/1 classes.")
parser.add_argument('--ds_rate', default=None, type=float,
                    help="Downsample rate of the datasets.")

args = parser.parse_args()

if not args.dataset:
    args.dataset = ['CleanSpeech']

if args.dirpath == '':
    args.dirpath = time.strftime("[%d-%b-%Y-%H:%M:%S]", time.localtime())
if os.path.isfile(args.dirpath):
    raise NameError
elif not os.path.exists(args.dirpath):
    os.mkdir(args.dirpath)

if args.recordpath == '':
    args.recordpath = "training_records.h5"

if args.parampath == '':
    args.parampath = "parameters"

if args.training_scale == -1:
    args.training_scale = None

if args.testing_scale == -1:
    args.testing_scale = None

if args.shift_batch_norm:
    BNLayer = shift_batch_norm.BatchNormSfhitPow2Layer
else:
    BNLayer = batch_norm.BatchNormLayer

def noise_generator_wrapper(lb, ub, repeat, balance_classes=True):
    """
    Input Arguments:
    ----------------
    lb: int
        lower bound of leakage spike

    ub: int
        upper bound of leakage spike

    repeat: int
        number of repeat for each example

    """
    def _noise_generator(x, y):
        if balance_classes:
            num_1 = sum(y)
            num_0 = len(y) - num_1

            major_class = 1 if num_1 > num_0 else 0
            minor_class = 1 - major_class

            num_diff = abs(num_1 - num_0)
            num_minor = (num_1 + num_0 - num_diff) // 2
            num_major = num_minor + num_diff
            num_null = num_major
            # num_diff = num_diff

            null_x = np.zeros((num_null, x.shape[1]))
            null_y = np.zeros(num_null, dtype=np.int)

            minor_idx = np.nonzero( y == minor_class )[0]

            random_idx = minor_idx[np.random.randint(0, num_minor, num_diff)]
            minor_x = x[random_idx,:]
            minor_y = np.full(num_diff, minor_class, dtype=np.int)

            x = np.vstack((x, null_x, minor_x))
            y = np.concatenate((y, null_y, minor_y))

        x = np.repeat(x, repeat, axis=0)
        y = np.repeat(y, repeat, axis=0)
        x += np.random.randint(lb, ub+1, x.shape)
        x = np.clip(x, 0, 255)
        idx = np.random.permutation(len(y))

        x = x[idx,:]
        y = y[idx]

        return x, y
    return _noise_generator


if __name__ == "__main__":

    # BN parameters
    print("batch_size = "+str(args.batch_size))
    # alpha is the exponential moving average factor
    alpha = .15
    print("alpha = "+str(alpha))
    epsilon = 1e-4
    print("epsilon = "+str(epsilon))
    print("batch noramlization = " + str(args.batch_norm))
    print("batch noramlization model = " + str(BNLayer.__name__))

    # MLP parameters
    print("hidden_layers = "+str(args.hidden_layers))
    print("context_window = "+str(args.context_window))

    # Training parameters
    print("num_epochs = "+str(args.num_epochs))

    # Dropout parameters
    print("dropout_in = "+str(args.dropout_in)) # 0. means no dropout
    print("dropout_hidden = "+str(args.dropout_hidden))

    # Dataset parameters
    print("training_scale = "+str(args.training_scale))
    print("testing_scale = "+str(args.testing_scale))

    # BinaryOut
    # activation = binary_net.binary_tanh_unit
    # args.activation = "binary_tanh_unit"
    activation = binary_net.binary_sigmoid_unit
    args.activation = "binary_sigmoid_unit"
    print("activation = "+str(args.activation))

    # activation = binary_net.my_binary_sigmoid_unit
    # print("activation = binary_net.my_binary_sigmoid_unit")

    # BinaryConnect
    binary = True
    print("binary = "+str(binary))
    stochastic = False
    print("stochastic = "+str(stochastic))
    # (-H,+H) are the two binary values
    # H = "Glorot"
    H = 1.
    print("H = "+str(H))
    # W_LR_scale = 1.
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))
    print("zero_bias = "+str(args.zero_bias))

    # Decaying LR
    LR_start = args.learning_rate
    print("LR_start = "+str(LR_start))
    LR_fin = args.learning_rate * 1e-3
    print("LR_fin = "+str(LR_fin))
    LR_decay = (LR_fin/LR_start)**(1./args.num_epochs)
    print("LR_decay = "+str(LR_decay))
    # BTW, LR decay might good for the BN moving average...

    # Train record
    args.parampath = os.path.join(args.dirpath, args.parampath)
    print("param_path = "+str(args.parampath))
    args.recordpath = os.path.join(args.dirpath, args.recordpath)
    print("file_path = "+str(args.recordpath))

    shuffle_parts = 1
    print("shuffle_parts = "+str(shuffle_parts))

    configpath =  os.path.join(args.dirpath, 'config')
    with open(configpath,'w') as f:
        for k,v in vars(args).items():
            f.write("%s=%s\n" % (str(k),str(v)))

    spike_leakage_noise = None
    if args.leakage_spike:
        spike_leakage_noise = noise_generator_wrapper(
            *args.leakage_spike,
            balance_classes=args.balance_classes)

    print('Loading voice dataset...')
    train_set, valid_set, test_sets = input_data.read_data_sets(
        args.dataset,
        scale_training=args.training_scale,
        scale_testing=args.testing_scale,
        onehot=args.onehot,
        hinge=args.hinge,
        context_window=args.context_window,
        prefix=args.prefix,
        testing=args.testing_dataset,
        noise_generator=spike_leakage_noise,
        ds_rate=args.ds_rate)

    print('Size of training set: %d' % train_set._num_examples)
    print('Size of validation set: %d' % valid_set._num_examples)
    for x, ds in test_sets:
        print('Size of testing set %s: %d' % (x, ds._num_examples))

    print('Building the MLP...')

    # Prepare Theano variables for inputs and targets
    input = T.matrix('inputs')
    target = T.matrix('targets')
    LR = T.scalar('LR', dtype=theano.config.floatX)

    mlp = lasagne.layers.InputLayer(
            shape=(None, 16*len(args.context_window)),
            input_var=input)

    mlp = lasagne.layers.DropoutLayer(
            mlp,
            p=args.dropout_in)

    for k in args.hidden_layers:

        mlp = binary_net.DenseLayer(
                mlp,
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=k)

        if args.batch_norm:
            mlp = BNLayer(
                    mlp,
                    epsilon=epsilon,
                    alpha=alpha)

        mlp = lasagne.layers.NonlinearityLayer(
                mlp,
                nonlinearity=activation)

        mlp = lasagne.layers.DropoutLayer(
                mlp,
                p=args.dropout_hidden)

    mlp = binary_net.DenseLayer(
                mlp,
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=2)

    if args.batch_norm:
        mlp = BNLayer(
                mlp,
                epsilon=epsilon,
                alpha=alpha)

    train_output = lasagne.layers.get_output(mlp, deterministic=False)


    # squared hinge loss
    # soft = T.nnet.softmax(train_output)
    # loss = T.mean(T.nnet.categorical_crossentropy(soft, target))
    loss = T.mean(T.sqr(T.maximum(0.,1.-target*train_output)))

    if args.zero_bias:
        B = lasagne.layers.get_all_params(mlp, binary=False)
        for b in B:
            loss += .1*T.mean(T.sqr(b))

    if binary:

        # W updates
        W = lasagne.layers.get_all_params(mlp, binary=True)
        W_grads = binary_net.compute_grads(loss,mlp)
        updates = lasagne.updates.adam(loss_or_grads=W_grads, params=W, learning_rate=LR)
        updates = binary_net.clipping_scaling(updates,mlp)

        # other parameters updates
        params = lasagne.layers.get_all_params(mlp, trainable=True, binary=False)
        updates = OrderedDict(updates.items() + lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR).items())

    else:
        params = lasagne.layers.get_all_params(mlp, trainable=True)
        updates = lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR)

    test_output = lasagne.layers.get_output(mlp, deterministic=True)

    test_soft = T.nnet.softmax(test_output)
    # test_loss = T.mean(T.nnet.categorical_crossentropy(test_soft, target))
    test_loss = T.mean(T.sqr(T.maximum(0.,1.-target*test_output)))
    test_err = T.mean(T.neq(T.argmax(test_output, axis=1), T.argmax(target, axis=1)),dtype=theano.config.floatX)
    # test_soft = T.nnet.softmax(test_output)
    test_score = test_soft[:,1]

    # Compile a function performing a training step on a mini-batch (by giving the updates dictionary)
    # and returning the corresponding training loss:
    train_fn = theano.function([input, target, LR], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input, target], [test_output, test_loss, test_err, test_score])

    print('Training...')

    with h5py.File(args.recordpath,"w") as fp:
        for k,v in vars(args).items():
            fp.attrs[k] = v if v is not None else str('Not Set')
        fp.attrs['filename'] = os.path.basename(sys.argv[0])

        binary_net.train(
                train_fn,val_fn,
                mlp,
                args.batch_size,
                LR_start,LR_decay,
                args.num_epochs,
                train_set.windows, train_set.labels,
                valid_set.windows, valid_set.labels,
                test_sets,
                args.parampath,
                shuffle_parts,
                fp=fp,
                zero_bias=args.zero_bias)
