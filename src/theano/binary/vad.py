# Copyright 2017 Chung-Heng Yeh

# This file is part of BinaryConnect.

# BinaryConnect is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# BinaryConnect is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with BinaryConnect.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import argparse
import sys
import os
import time
import h5py

import numpy as np
np.random.seed(1234)  # for reproducibility

# specifying the gpu to use
# import theano.sandbox.cuda
# theano.sandbox.cuda.use('gpu1')
import theano
import theano.tensor as T

import lasagne

import cPickle as pickle
import gzip

from collections import OrderedDict

import batch_norm
import binary_connect
import input_data

parser = argparse.ArgumentParser()

parser.add_argument('--hinge', default=True, type=bool,
                    help='initial learning rate.')
parser.add_argument('--onehot', default=True, type=bool,
                    help='Whether to.')
parser.add_argument('--learning_rate', default=0.01, type=float,
                    help='initial learning rate.')
parser.add_argument('--dropout_in', default=0.0, type=float,
                    help='Drop out rate of input data.')
parser.add_argument('--dropout_hidden', default=0.0, type=float,
                    help='Drop out rate of hidden layer.')
parser.add_argument('--num_epochs', default=2000, type=int,
                    help='Number of epochs.')
parser.add_argument('--hidden_layers', default=[128,32], type=int, nargs='*',
                    help='A list of number of units in each hidden layers, ex. 128, 32.')
parser.add_argument('--batch_size', default=200, type=int,
                    help='Batch size.  Must divide evenly into the dataset sizes.')
parser.add_argument('--train_dir', default='data', type=str,
                    help='Directory to put the training data.')
parser.add_argument('--context_window', default=[-3, 0, 3], type=int, nargs='*',
                    help='Context window, a string representing a list of '
                         'integers speparated by commas.')
parser.add_argument('--prefix', default='', type=str,
                    help='Prefix to the dataset.')
parser.add_argument('--filepath', default='./', type=str,
                    help='Path to file.')
parser.add_argument('--filename', default='', type=str,
                    help='File name.')
parser.add_argument('--dataset', default=[], help='dataset name', action='append')

args = parser.parse_args()

if not args.dataset:
    args.dataset = ['CleanSpeech']

if args.filename == '':
    args.filename = time.strftime("%d-%b-%Y-%H:%M:%S.h5", time.localtime())

if __name__ == "__main__":

    print(args)

    # BN parameters
    print("batch_size = "+str(args.batch_size))
    # alpha is the exponential moving average factor
    alpha = .15
    print("alpha = "+str(alpha))
    epsilon = 1e-4
    print("epsilon = "+str(epsilon))

    # MLP parameters
    print("n_hidden_layers = "+str(args.hidden_layers))

    # Training parameters
    print("num_epochs = "+str(args.num_epochs))

    # Dropout parameters
    print("dropout_in = "+str(args.dropout_in)) # 0. means no dropout
    print("dropout_hidden = "+str(args.dropout_hidden))

    # BinaryConnect
    binary = True
    print("binary = "+str(binary))
    stochastic = True
    print("stochastic = "+str(stochastic))
    # (-H,+H) are the two binary values
    # H = "Glorot"
    H = 1.
    print("H = "+str(H))
    # W_LR_scale = 1.
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))

    # Decaying LR
    LR_start = .001
    print("LR_start = "+str(LR_start))
    LR_fin = 0.000003
    print("LR_fin = "+str(LR_fin))
    LR_decay = (LR_fin/LR_start)**(1./args.num_epochs)
    print("LR_decay = "+str(LR_decay))
    # BTW, LR decay might good for the BN moving average...

    print('Loading voice dataset...')
    train_set, valid_set, test_set = input_data.read_data_sets(
        args.dataset,
        args.train_dir,
        onehot=args.onehot,
        hinge=args.hinge,
        context_window=args.context_window,
        prefix=args.prefix)

    print('Building the MLP...')

    # Prepare Theano variables for inputs and targets
    input = T.matrix('inputs')
    target = T.matrix('targets')
    LR = T.scalar('LR', dtype=theano.config.floatX)

    mlp = lasagne.layers.InputLayer(
            shape=(None, 16*len(args.context_window)),
            input_var=input)

    mlp = lasagne.layers.DropoutLayer(
            mlp,
            p=args.dropout_in)

    for k in args.hidden_layers:

        mlp = binary_connect.DenseLayer(
                mlp,
                binary=binary,
                stochastic=stochastic,
                H=H,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=k)

        mlp = batch_norm.BatchNormLayer(
                mlp,
                epsilon=epsilon,
                alpha=alpha,
                nonlinearity=lasagne.nonlinearities.rectify)

        mlp = lasagne.layers.DropoutLayer(
                mlp,
                p=args.dropout_hidden)

    mlp = binary_connect.DenseLayer(
                mlp,
                binary=binary,
                stochastic=stochastic,
                H=H,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=2)

    mlp = batch_norm.BatchNormLayer(
            mlp,
            epsilon=epsilon,
            alpha=alpha,
            nonlinearity=lasagne.nonlinearities.identity)

    train_output = lasagne.layers.get_output(mlp, deterministic=False)

    # squared hinge loss
    loss = T.mean(T.sqr(T.maximum(0.,1.-target*train_output)))

    if binary:

        # W updates
        W = lasagne.layers.get_all_params(mlp, binary=True)
        W_grads = binary_connect.compute_grads(loss,mlp)
        updates = lasagne.updates.adam(loss_or_grads=W_grads, params=W, learning_rate=LR)
        updates = binary_connect.clipping_scaling(updates,mlp)

        # other parameters updates
        params = lasagne.layers.get_all_params(mlp, trainable=True, binary=False)
        updates = OrderedDict(updates.items() + lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR).items())

    else:
        params = lasagne.layers.get_all_params(mlp, trainable=True)
        updates = lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR)

    test_output = lasagne.layers.get_output(mlp, deterministic=True)
    test_loss = T.mean(T.sqr(T.maximum(0.,1.-target*test_output)))
    test_err = T.mean(T.neq(T.argmax(test_output, axis=1), T.argmax(target, axis=1)),dtype=theano.config.floatX)
    test_soft = T.nnet.softmax(test_output)
    test_score = test_soft[:,1]

    # Compile a function performing a training step on a mini-batch (by giving the updates dictionary)
    # and returning the corresponding training loss:
    train_fn = theano.function([input, target, LR], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input, target], [test_loss, test_err, test_score])

    print('Training...')

    with h5py.File(args.filepath+args.filename,"w") as fp:
        for k,v in vars(args).items():
            fp.attrs[k] = v
        fp.attrs['filename'] = os.path.basename(sys.argv[0])

        binary_connect.train(
                train_fn,val_fn,
                args.batch_size,
                LR_start,LR_decay,
                args.num_epochs,
                train_set.windows, train_set.labels,
                valid_set.windows,valid_set.labels,
                test_set.windows,test_set.labels,
                fp=fp)

    # print("display histogram")

    # W = lasagne.layers.get_all_layers(mlp)[2].W.get_value()
    # print(W.shape)

    # histogram = np.histogram(W,bins=1000,range=(-1.1,1.1))
    # np.savetxt(str(dropout_hidden)+str(binary)+str(stochastic)+str(H)+"_hist0.csv", histogram[0], delimiter=",")
    # np.savetxt(str(dropout_hidden)+str(binary)+str(stochastic)+str(H)+"_hist1.csv", histogram[1], delimiter=",")

    # Optionally, you could now dump the network weights to a file like this:
    # np.savez('model.npz', lasagne.layers.get_all_param_values(network))
