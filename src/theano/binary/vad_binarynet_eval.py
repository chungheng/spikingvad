
from __future__ import print_function

import argparse
import sys
import os
import time
import h5py

import numpy as np
np.random.seed(1234)  # for reproducibility

# specifying the gpu to use
# import theano.sandbox.cuda
# theano.sandbox.cuda.use('gpu1')
import theano
import theano.tensor as T

import lasagne

import cPickle as pickle
import gzip


from collections import OrderedDict

import binary_net
import batch_norm
import input_data
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def save_pdf(filename, h5):

    config = h5.attrs

    dirname = config['filepath'].split('/')[-2]
    ds = config['dataset'][0]
    num_epochs = len(config['epochs'])

    cmap = plt.get_cmap('hsv')

    fig = plt.figure(figsize=(8.27,11.69))

    fig.suptitle('Analysis of %s in %s' % (ds, dirname),
        fontsize=14, fontweight='bold')

    ax1 = fig.add_subplot(111)


    for i, epoch in enumerate(config['epochs']):
        color = cmap(float(i) / float(num_epochs) )
        label = 'Epoch %d' % epoch
        for k,v in h5.items():
            ax1.scatter(v['tp'][i],v['tn'][i], color=color, label=label)
            ax1.annotate(" v%s" % (k), (v['tp'][i],v['tn'][i]))
            label = None

    ax1.legend()

    # plt.tight_layout()
    fig.subplots_adjust(top=0.9)

    ax1.set_xlim([0,1])
    ax1.set_ylim([0,1])
    ax1.set_xlabel('TP')
    ax1.set_ylabel('TN')
    ax1.set_title('TP/TN Distribution across Epochs and Versions')
    ax1.grid()

    plt.savefig('%s.pdf' % filename, dpi=300)

def save_csv(filepath, h5):
    config = h5.attrs

    d = OrderedDict((
        ('Epoch',[]),
        ('Version',[]),
        ('TP',[]),
        ('TN', []),
        ('AUC',[])))
    for i, epoch in enumerate(config['epochs']):
        for k,v in h5.items():
            d['Epoch'].append(epoch)
            d['Version'].append(k)
            d['TP'].append(v['tp'][i])
            d['TN'].append(v['tn'][i])
            d['AUC'].append(v['auc'][i])
    df = pd.DataFrame(d)
    df.to_csv('%s.csv' % filepath, index=False)

parser = argparse.ArgumentParser()

parser.add_argument('--epochs', default=[1], type=int, nargs='*',
                    help='Indices of epochs to be tested.')
parser.add_argument('--prefix', default='', type=str,
                    help='Prefix to the dataset.')
parser.add_argument('dirpath', default='', type=str,
                    help='directoy to the training record and the model parameters.')
parser.add_argument('--filepath', default='', type=str,
                    help='File name to save testing progress.')
parser.add_argument('--testing_dataset', default=[], type=str, nargs='*',
                    help='testing dataset version.')


args = parser.parse_args()


if args.filepath == '':
    args.filepath = "testing_records"



config = {}
configpath =  os.path.join(args.dirpath, 'config')
with open(configpath,'r') as f:
    for line in f:
        seg = [x.strip() for x in line.split('=')]
        try:
            a = eval(seg[1])
        except:
            a = seg[1]
        finally:
            config[seg[0]] = a

for k,v in config.items():
    print("%s=%s" % (k,v))

if __name__ == "__main__":
    #
    # BN parameters
    alpha = .15
    print("alpha = "+str(alpha))
    epsilon = 1e-4
    print("epsilon = "+str(epsilon))
    # alpha is the exponential moving average factor
    # print("batch noramlization = " + str(config["batch_norm"]))
    #
    # # MLP parameters
    # print("hidden_layers = "+str(config["hidden_layers"]))
    # print("context_window = "+str(config["context_window"]))
    #
    # # Training parameters
    # print("num_epochs = "+str(config["num_epochs"]))
    #
    # # Dataset parameters
    # print("training_scale = "+str(args.training_scale))
    # print("testing_scale = "+str(args.testing_scale))
    #
    # BinaryOut
    if config['activation'] == 'binary_tanh_unit':
        activation = binary_net.binary_tanh_unit
    elif config['activation'] == 'binary_sigmoid_unit':
        activation = binary_net.binary_sigmoid_unit
    #
    # activation = binary_net.my_binary_sigmoid_unit
    # print("activation = binary_net.my_binary_sigmoid_unit")

    # BinaryConnect
    binary = True
    print("binary = "+str(binary))
    stochastic = False
    print("stochastic = "+str(stochastic))
    # (-H,+H) are the two binary values
    # H = "Glorot"
    H = 1.
    print("H = "+str(H))
    # W_LR_scale = 1.
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))
    print("zero_bias = "+str(config["zero_bias"]))
    #
    # # Train record
    # args.parampath = os.path.join(args.dirpath, args.parampath)
    # print("param_path = "+str(args.parampath))
    # #args.filepath = os.path.join(args.dirpath, args.label, args.filepath)
    # print("file_path = "+str(args.filepath))

    # Testing record
    args.filepath = os.path.join(args.dirpath, args.filepath)
    print("file_path.[pdf/h5/txt] = "+args.filepath)

    print('Loading voice dataset...')
    train_set, valid_set, test_sets = input_data.read_data_sets(
        config["dataset"],
        scale_training=config["training_scale"],
        scale_testing=config["testing_scale"],
        onehot=config["onehot"],
        hinge=config["hinge"],
        context_window=config["context_window"],
        prefix=config["prefix"],
        testing=args.testing_dataset)

    print('Building the MLP...')

    # Prepare Theano variables for inputs and targets
    input = T.matrix('inputs')
    target = T.matrix('targets')
    LR = T.scalar('LR', dtype=theano.config.floatX)

    mlp = lasagne.layers.InputLayer(
            shape=(None, 16*len(config['context_window'])),
            input_var=input)

    mlp = lasagne.layers.DropoutLayer(
            mlp,
            p=config["dropout_in"])

    for k in config["hidden_layers"]:

        mlp = binary_net.DenseLayer(
                mlp,
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=k)

        if config["batch_norm"]:
            mlp = batch_norm.BatchNormLayer(
                    mlp,
                    epsilon=epsilon,
                    alpha=alpha)

        mlp = lasagne.layers.NonlinearityLayer(
                mlp,
                nonlinearity=activation)

        mlp = lasagne.layers.DropoutLayer(
                mlp,
                p=config["dropout_hidden"])

    mlp = binary_net.DenseLayer(
                mlp,
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=2)

    if config["batch_norm"]:
        mlp = batch_norm.BatchNormLayer(
                mlp,
                epsilon=epsilon,
                alpha=alpha)

    train_output = lasagne.layers.get_output(mlp, deterministic=False)


    # squared hinge loss
    # soft = T.nnet.softmax(train_output)
    # loss = T.mean(T.nnet.categorical_crossentropy(soft, target))
    loss = T.mean(T.sqr(T.maximum(0.,1.-target*train_output)))

    if config["zero_bias"]:
        B = lasagne.layers.get_all_params(mlp, binary=False)
        for b in B:
            loss += .1*T.mean(T.sqr(b))

    if binary:

        # W updates
        W = lasagne.layers.get_all_params(mlp, binary=True)
        W_grads = binary_net.compute_grads(loss,mlp)
        updates = lasagne.updates.adam(loss_or_grads=W_grads, params=W, learning_rate=LR)
        updates = binary_net.clipping_scaling(updates,mlp)

        # other parameters updates
        params = lasagne.layers.get_all_params(mlp, trainable=True, binary=False)
        updates = OrderedDict(updates.items() + lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR).items())

    else:
        params = lasagne.layers.get_all_params(mlp, trainable=True)
        updates = lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR)

    test_output = lasagne.layers.get_output(mlp, deterministic=True)

    test_soft = T.nnet.softmax(test_output)
    # test_loss = T.mean(T.nnet.categorical_crossentropy(test_soft, target))
    test_loss = T.mean(T.sqr(T.maximum(0.,1.-target*test_output)))
    test_err = T.mean(T.neq(T.argmax(test_output, axis=1), T.argmax(target, axis=1)),dtype=theano.config.floatX)
    # test_soft = T.nnet.softmax(test_output)
    test_score = test_soft[:,1]

    # Compile a function performing a training step on a mini-batch (by giving the updates dictionary)
    # and returning the corresponding training loss:
    train_fn = theano.function([input, target, LR], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input, target], [test_output, test_loss, test_err, test_score])

    print('Evaluating...')

    with h5py.File('%s.h5' % args.filepath,"w") as fp:
        for k,v in vars(args).items():
            fp.attrs[k] = v if v is not None else str('Not Set')
        for k,v in config.items():
            fp.attrs[k] = v if v is not None else str('Not Set')
        fp.attrs['filename'] = os.path.basename(sys.argv[0])

        binary_net.eval(
                train_fn,val_fn,
                mlp,
                config["batch_size"],
                config["num_epochs"],
                args.epochs,
                test_sets,
                config["parampath"],
                fp=fp,
                zero_bias=config["zero_bias"])
        save_pdf(args.filepath, fp)
        save_csv(args.filepath, fp)
