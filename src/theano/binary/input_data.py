# Copyright 2017 Chung-Heng Yeh

# This file is part of Spike VAD

# BinaryConnect is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# BinaryConnect is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with BinaryConnect.  If not, see <http://www.gnu.org/licenses/>.
"""Functions for reading VAD data."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gzip
import os
import tempfile

import numpy
import h5py
import binascii
import time
from itertools import product
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin

numpy.random.seed(int(time.time()))

def load_data_x(filepath):
    with h5py.File(filepath,'r') as f:
        x = numpy.asarray(f.get(u'real')).T
    x = x[:-3,:]
    return x

def load_data_y(filepath):
    with open(filepath,'rb') as f:
        y = numpy.asarray([int(binascii.hexlify(b)) for b in f.read()])
    return y

def process_filepath(training_sets, prefix='', testing_sets=[]):
    """
    parse dataset to filepath on system

    input:
    training_sets: list of strings
        ex. ['CleanSpeech/Q2', 'Noisy/Q2/Bus/-5,10']

    return: list of strings
        return list of filepaths
    """
    def process_featureQ(featureQ):
        _featureQ_list = ["{0:05b}".format(x) for x in xrange(32)]
        if featureQ == "*":
            featureQ = _featureQ_list
        else:
            featureQ = [x.strip(' ') for x in featureQ.split(',')]

        output = []
        for x in featureQ:
            if x not in _featureQ_list:
                print('Feature' + x + ' not available')
            else:
                output.append('Feature' + x)
        return output

    def process_version(version):
        _version_list = [str(x) for x in range(1,201)]
        if version == "*":
            version = _version_list
        else:
            version = [x.replace('v','').strip(' ') for x in version.split(',')]

        output = []
        for x in version:
            if x not in _version_list:
                print('v' + x + ' not available')
            else:
                output.append('v' + x)
        return output

    def process_noise(noise):
        _noise_list = ['bus','cafe','cafeteria','car','field','metro','park',
            'restaurant','river','square','station','traffic']
        if noise == "*":
            noise = _noise_list
        else:
            noise = [x.strip(' ') for x in noise.split(',')]

        output = []
        for x in noise:
            if x not in _noise_list:
                print('Noise Type ' + x + ' not available')
            else:
                output.append(x)
        return output

    def process_db(db):
        _db_list = ['-5','0','5','10','15']
        if db == "*":
            db = _db_list
        else:
            db = [x.strip(' ') for x in db.split(',')]

        output = []
        for x in db:
            if x not in _db_list:
                print('Decibel Value ' + x + ' not available')
            else:
                output.append(x)
        return output

    def process_cleanspeech(seg):
        output = [['CleanSpeech'],None,['%stotN.h5']]
        if len(seg) == 1:
            seg.append('*')
        output[1] = process_featureQ( seg[1].strip(' ') )
        return output

    def process_noisyspeech(seg):
        output = [['NoisySpeech'],['%s'],None,None,[]]
        if len(seg) == 1:
            seg.append('*')
        output[2] = process_featureQ( seg[1].strip(' ') )

        # process version
        if len(seg) == 2:
            seg.append('*')
        output[3]  = process_version( seg[2].strip(' ') )

        # process noise types
        if len(seg) == 3:
            seg.append('*')
        noise_types = process_noise( seg[3].strip(' ') )

        # process noise types
        if len(seg) == 4:
            seg.append('*')
        db_types = process_db( seg[4].strip(' ') )
        output[4] = [n+d+'dB.h5' for n in noise_types for d in db_types]
        return output

    processed_training_sets = []
    for ds in training_sets:
        seg = ds.split('/')
        c = seg[0][0].lower()
        if c == 'c':
            seg = process_cleanspeech(seg)
        elif c == 'n':
            seg = process_noisyspeech(seg)
        else:
            raise ValueError
        processed_training_sets.append(seg)

    training_output = []
    testing_output = []
    for seg in processed_training_sets:
        for x in product(*seg):
            abs_path = os.path.join(prefix, *x)
            training_output.append(abs_path)

    for version in testing_sets:
        testing_output.append([version, []])
        for seg in processed_training_sets:
            a = seg[:]
            a[3] = ['v' + version.strip()]
            for x in product(*a):
                abs_path = os.path.join(prefix, *x)
                testing_output[-1][1].append(abs_path)

    if not testing_output:
        testing_output.append(['test',training_output])

    print(training_output)
    print(testing_output)

    return training_output, testing_output

class DataSet(object):

    def __init__(self,
                 windows,
                 labels,
                 scale=None,
                 onehot=False,
                 hinge=False,
                 dtype=numpy.float32,
                 context_window=None,
                 noise_generator=None,
                 ds_rate=None):
        """Construct a DataSet.
        one_hot arg is used only if fake_data is true.  `dtype` can be either
        `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
        `[0, 1]`.
        """
        if dtype not in (numpy.uint8, numpy.float32):
            raise TypeError('Invalid window dtype %r, expected uint8 or float32' %
                          dtype)

        assert windows.shape[0] == labels.shape[0], (
            'windows.shape: %s labels.shape: %s' % (windows.shape, labels.shape))
        self._num_examples = windows.shape[0]

        if dtype == numpy.float32:
            # Convert from [0, 255] -> [0.0, 1.0].
            windows = windows.astype(numpy.float32)
            #windows = numpy.multiply(windows, 1.0 / 255.0)
        if scale:
            windows = windows / scale

        self._context_window = context_window or 0
        self._windows = windows
        self._labels = labels.copy()
        self._epochs_completed = 0
        self._index_in_epoch = 0

        if self._context_window is not 0:
            if 0 not in self._context_window:
                raise ValueError("Context window should contain 0")

            cw = numpy.asarray(self._context_window)
            cw.sort()

            if (cw[0] > 0) or (cw[-1] < 0):
                raise ValueError("Invalid context window range")

            begin_idx = -cw[0]
            end_idx = self._windows.shape[0] - cw[-1]

            self._labels = self._labels[begin_idx:end_idx]
            self._windows = numpy.hstack([self._windows[(begin_idx+i):(end_idx+i),:] for i in cw])

            self._num_examples = self._windows.shape[0]

        if ds_rate is not None:
            index = numpy.random.permutation(self._num_examples)
            self._num_examples = int(self._num_examples // ds_rate)
            index = index[:self._num_examples]

            self._windows = self._windows[index,:]
            self._labels = self._labels[index]

        if noise_generator is not None:
            self._windows, self._labels = noise_generator(self._windows, self._labels)
            self._num_examples = self._windows.shape[0]

        if onehot:
            self._labels = numpy.float32(numpy.eye(2)[self._labels])

        if hinge:
            self._labels = 2*self._labels - 1.

    @classmethod
    def merge(cls, datasets):
         def checkEqualIvo(lst):
             return not lst or lst.count(lst[0]) == len(lst)
         assert(checkEqualIvo([x._context_window for x in datasets]))
         assert(checkEqualIvo([x._windows.shape[1] for x in datasets]))

         windows = numpy.vstack([x._windows for x in datasets])
         labels = numpy.vstack([x._labels for x in datasets])

         newDataSet = cls(windows, labels)
         newDataSet._context_window = datasets[0]._context_window
         return newDataSet

    @property
    def windows(self):
        return self._windows

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size, fake_data=False):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = numpy.arange(self._num_examples)
            numpy.random.shuffle(perm)
            self._windows = self._windows[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._windows[start:end], self._labels[start:end]

def read_data_sets(data_set,
                   onehot=False,
                   hinge=False,
                   scale_training = None,
                   scale_testing = None,
                   dtype=numpy.float32,
                   context_window=None,
                   validation_size=5000,
                   prefix="../../../",
                   testing=[],
                   noise_generator=None,
                   ds_rate=None):

    training_y = load_data_y(os.path.join(prefix,'CleanSpeech/Labels/trainingtot.vad'))
    validation_y = load_data_y(os.path.join(prefix,'CleanSpeech/Labels/evaluationtot.vad'))
    testing_y = load_data_y(os.path.join(prefix,'CleanSpeech/Labels/testingtot.vad'))

    training_sets, testing_sets = process_filepath(data_set, prefix, testing)

    training_raw = []
    validation_raw = []

    for fp in training_sets:
        # load testing dataset
        x = load_data_x(fp % 'training')
        training_raw.append(DataSet(x, training_y, scale=scale_training,
            onehot=onehot, hinge=hinge, dtype=dtype,
            context_window=context_window, noise_generator=noise_generator,
            ds_rate=ds_rate))

        # load validation dataset
        x = load_data_x(fp % 'evaluation')
        validation_raw.append(DataSet(x, validation_y, scale=scale_training,
            onehot=onehot, hinge=hinge, dtype=dtype,
            context_window=context_window, ds_rate=ds_rate))

    train = DataSet.merge(training_raw)
    validation = DataSet.merge(validation_raw)

    test = []
    for n,s in testing_sets:
        testing_raw = []

        for fp in s:
            print(fp)
            # load testing dataset
            x = load_data_x(fp % 'testing')
            testing_raw.append(
                DataSet(x, testing_y, scale=scale_testing, onehot=onehot,
                    hinge=hinge, dtype=dtype,
                    context_window=context_window, ds_rate=ds_rate))

        test.append([n,DataSet.merge(testing_raw)])

    return train, validation, test
