
import time, os

from collections import OrderedDict

import math
import numpy as np
import h5py

# specifying the gpu to use
# import theano.sandbox.cuda
# theano.sandbox.cuda.use('gpu1')
from sklearn.metrics import roc_auc_score
import theano
import theano.tensor as T

import lasagne

from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from theano.scalar.basic import UnaryScalarOp, same_out_nocomplex
from theano.tensor.elemwise import Elemwise

from batch_norm import *
from monitor import Monitor

# Our own rounding function, that does not set the gradient to 0 like Theano's
class Round3(UnaryScalarOp):

    def c_code(self, node, name, (x,), (z,), sub):
        return "%(z)s = round(%(x)s);" % locals()

    def grad(self, inputs, gout):
        (gz,) = gout
        return gz,

round3_scalar = Round3(same_out_nocomplex, name='round3')
round3 = Elemwise(round3_scalar)

def hard_sigmoid(x):
    return T.clip((x+1.)/2.,0,1)

# The neurons' activations binarization function
# It behaves like the sign function during forward propagation
# And like:
#   hard_tanh(x) = 2*hard_sigmoid(x)-1
# during back propagation
def binary_tanh_unit(x):
    return 2.*round3(hard_sigmoid(x))-1.

def binary_sigmoid_unit(x):
    return round3(hard_sigmoid(x))

# The weights' binarization function,
# taken directly from the BinaryConnect github repository
# (which was made available by his authors)
def binarization(W,H,binary=True,deterministic=False,stochastic=False,srng=None):

    # (deterministic == True) <-> test-time <-> inference-time
    if not binary or (deterministic and stochastic):
        # print("not binary")
        Wb = W

    else:

        # [-1,1] -> [0,1]
        Wb = hard_sigmoid(W/H)
        # Wb = T.clip(W/H,-1,1)

        # Stochastic BinaryConnect
        if stochastic:

            # print("stoch")
            Wb = T.cast(srng.binomial(n=1, p=Wb, size=T.shape(Wb)), theano.config.floatX)

        # Deterministic BinaryConnect (round to nearest)
        else:
            # print("det")
            Wb = T.round(Wb)

        # 0 or 1 -> -1 or 1
        Wb = T.cast(T.switch(Wb,H,-H), theano.config.floatX)

    return Wb

# This class extends the Lasagne DenseLayer to support BinaryConnect
class DenseLayer(lasagne.layers.DenseLayer):

    def __init__(self, incoming, num_units,
        binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", **kwargs):

        self.binary = binary
        self.stochastic = stochastic

        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            self.H = np.float32(np.sqrt(1.5/ (num_inputs + num_units)))
            # print("H = "+str(self.H))

        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(incoming.output_shape[1:]))
            self.W_LR_scale = np.float32(1./np.sqrt(1.5/ (num_inputs + num_units)))

        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))

        if self.binary:
            super(DenseLayer, self).__init__(incoming, num_units, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)
            # add the binary tag to weights
            self.params[self.W]=set(['binary'])

        else:
            super(DenseLayer, self).__init__(incoming, num_units, **kwargs)

    def get_output_for(self, input, deterministic=False, **kwargs):

        self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        Wr = self.W
        self.W = self.Wb

        rvalue = super(DenseLayer, self).get_output_for(input, **kwargs)

        self.W = Wr

        return rvalue

# This class extends the Lasagne Conv2DLayer to support BinaryConnect
class Conv2DLayer(lasagne.layers.Conv2DLayer):

    def __init__(self, incoming, num_filters, filter_size,
        binary = True, stochastic = True, H=1.,W_LR_scale="Glorot", **kwargs):

        self.binary = binary
        self.stochastic = stochastic

        self.H = H
        if H == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.H = np.float32(np.sqrt(1.5 / (num_inputs + num_units)))
            # print("H = "+str(self.H))

        self.W_LR_scale = W_LR_scale
        if W_LR_scale == "Glorot":
            num_inputs = int(np.prod(filter_size)*incoming.output_shape[1])
            num_units = int(np.prod(filter_size)*num_filters) # theoretically, I should divide num_units by the pool_shape
            self.W_LR_scale = np.float32(1./np.sqrt(1.5 / (num_inputs + num_units)))
            # print("W_LR_scale = "+str(self.W_LR_scale))

        self._srng = RandomStreams(lasagne.random.get_rng().randint(1, 2147462579))

        if self.binary:
            super(Conv2DLayer, self).__init__(incoming, num_filters, filter_size, W=lasagne.init.Uniform((-self.H,self.H)), **kwargs)
            # add the binary tag to weights
            self.params[self.W]=set(['binary'])
        else:
            super(Conv2DLayer, self).__init__(incoming, num_filters, filter_size, **kwargs)

    def convolve(self, input, deterministic=False, **kwargs):

        self.Wb = binarization(self.W,self.H,self.binary,deterministic,self.stochastic,self._srng)
        Wr = self.W
        self.W = self.Wb

        rvalue = super(Conv2DLayer, self).convolve(input, **kwargs)

        self.W = Wr

        return rvalue

# This function computes the gradient of the binary weights
def compute_grads(loss,network):

    layers = lasagne.layers.get_all_layers(network)
    grads = []

    for layer in layers:

        params = layer.get_params(binary=True)
        if params:
            # print(params[0].name)
            grads.append(theano.grad(loss, wrt=layer.Wb))

    return grads

# This functions clips the weights after the parameter update
def clipping_scaling(updates,network):

    layers = lasagne.layers.get_all_layers(network)
    updates = OrderedDict(updates)

    for layer in layers:

        params = layer.get_params(binary=True)
        for param in params:
            print("W_LR_scale = "+str(layer.W_LR_scale))
            print("H = "+str(layer.H))
            updates[param] = param + layer.W_LR_scale*(updates[param] - param)
            updates[param] = T.clip(updates[param], -layer.H,layer.H)

    return updates

# Given a dataset and a model, this function trains the model on the dataset for several epochs
# (There is no default trainer function in Lasagne yet)
def train(train_fn,val_fn,
            model,
            batch_size,
            LR_start,LR_decay,
            num_epochs,
            X_train,y_train,
            X_val,y_val,
            test_sets,
            save_path=None,
            shuffle_parts=1,
            fp=None,
            zero_bias=False):

    # A function which shuffles a dataset
    def shuffle(X,y):

        # print(len(X))

        chunk_size = len(X)/shuffle_parts
        shuffled_range = range(chunk_size)

        X_buffer = np.copy(X[0:chunk_size])
        y_buffer = np.copy(y[0:chunk_size])

        for k in range(shuffle_parts):

            np.random.shuffle(shuffled_range)

            for i in range(chunk_size):

                X_buffer[i] = X[k*chunk_size+shuffled_range[i]]
                y_buffer[i] = y[k*chunk_size+shuffled_range[i]]

            X[k*chunk_size:(k+1)*chunk_size] = X_buffer
            y[k*chunk_size:(k+1)*chunk_size] = y_buffer

        return X,y

        # shuffled_range = range(len(X))
        # np.random.shuffle(shuffled_range)

        # new_X = np.copy(X)
        # new_y = np.copy(y)

        # for i in range(len(X)):

            # new_X[i] = X[shuffled_range[i]]
            # new_y[i] = y[shuffled_range[i]]

        # return new_X,new_y

    # This function trains the model a full epoch (on the whole dataset)
    def train_epoch(X,y,LR):

        loss = 0
        batches = len(X)/batch_size

        for i in range(batches):
            loss += train_fn(X[i*batch_size:(i+1)*batch_size],y[i*batch_size:(i+1)*batch_size],LR)

        loss/=batches

        return loss

    # This function tests the model a full epoch (on the whole dataset)
    def val_epoch(X=None,y=None):
        if X is None and y is None:
            return ['auc','f_score','tp','tn','mcc']

        err = 0
        loss = 0
        batches = len(X)/batch_size
        score = []
        output = []

        for i in range(batches):
            new_output, new_loss, new_err, new_score = val_fn(X[i*batch_size:(i+1)*batch_size], y[i*batch_size:(i+1)*batch_size])
            err += new_err
            loss += new_loss
            score.append(new_score)
            output.extend(new_output)

        output = np.vstack(output)
        err = err / batches * 100
        loss /= batches
        score = np.concatenate(score)
        labels = np.argmax(y[:batches*batch_size], axis=1)
        output = np.vstack(output)
        predicts = np.argmax(output, axis=1)


        auc = roc_auc_score(labels, score)

        pos_tot = np.sum(labels).astype(np.float)
        neg_tot = len(labels) - pos_tot

        tp = np.sum(np.logical_and(predicts == 1, labels == 1))
        tn = np.sum(np.logical_and(predicts == 0, labels == 0))
        fn = pos_tot - tp
        fp = neg_tot - tn

        precision = tp / (tp + fp)
        recall = tp / pos_tot
        f_score = 2.*(precision*recall) / (precision+recall)

        mcc = ( tp*tn - fp*fn ) / np.sqrt( (tp+fp)*(tp+fn)*(tn+fp)*(tn+fn) )
        # return err, loss, auc, (tp, tn), f_score, mcc
        return {'auc':auc, 'f_score':f_score, 'tp':tp/pos_tot, 'tn':tn/neg_tot, 'mcc': mcc}

    # shuffle the train set
    X_train,y_train = shuffle(X_train,y_train)

    if save_path:
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        save_path = os.path.join(save_path,
            "[{0:0%d}]" % (math.floor(math.log(num_epochs, 10))+1))

    param_values = None
    LR = LR_start

    ds = OrderedDict([
        ('training', {
            'dataset': None,
            'metrics': ['loss','auc','mcc','f_score','tp','tn',"LR"],
        }),
        ('validation', {
            'dataset': (X_val, y_val),
            "criterions": [
                ('auc', 'largest', 'all'),
                # ('mcc', 'largest', 'all'),
                # ('f_score', 'largest', 'all')
            ]
        })
    ])
    for k,d in test_sets:
        ds[k] = {
            "dataset": (d.windows, d.labels),
            "criterions": [
                ('auc',"largest","alone"),
                # ('mcc',"largest","alone"),
                # ('f_score',"largest","alone"),
            ]
        }

    monitor = Monitor(
        epochs = num_epochs,
        datasets = ds,
        funcs = [val_epoch],
        h5fp = fp
    )

    # We iterate over epochs:
    for epoch in xrange(1, 1+num_epochs):

        start_time = time.time()

        train_loss = train_epoch(X_train,y_train,LR)
        train_record = val_epoch(X_train,y_train)
        train_record["loss"] = train_loss
        train_record["LR"] = LR

        epoch_duration = time.time() - start_time

        if zero_bias:

            param_values = lasagne.layers.get_all_param_values(model, binary=False)
            new_values = [np.zeros_like(x) for x in param_values]
            #new_values[-1] = param_values[-1]
            lasagne.layers.set_all_param_values(model, new_values, binary=False)

        monitor.update(epoch_duration, training=train_record)

        if zero_bias:
            lasagne.layers.set_all_param_values(model, param_values, binary=False)

        if save_path:
            filename = save_path.format(epoch)
            params = lasagne.layers.get_all_param_values(model)
            np.savez(filename, **{str(i):x for i,x in enumerate(params)})

        # decay the LR
        LR *= LR_decay

        # shuffle the training dataset
        X_train,y_train = shuffle(X_train,y_train)

        #
def eval(train_fn,val_fn,
            model,
            batch_size,
            num_epochs,
            epochs,
            test_sets,
            load_path=None,
            fp=None,
            zero_bias=False):

    # This function tests the model a full epoch (on the whole dataset)
    def val_epoch(X=None,y=None):
        if X is None and y is None:
            return ['auc','f_score','tp','tn','mcc']

        err = 0
        loss = 0
        batches = len(X)/batch_size
        score = []
        output = []

        for i in range(batches):
            new_output, new_loss, new_err, new_score = val_fn(X[i*batch_size:(i+1)*batch_size], y[i*batch_size:(i+1)*batch_size])
            err += new_err
            loss += new_loss
            score.append(new_score)
            output.extend(new_output)

        output = np.vstack(output)
        err = err / batches * 100
        loss /= batches
        score = np.concatenate(score)
        labels = np.argmax(y[:batches*batch_size], axis=1)
        output = np.vstack(output)
        predicts = np.argmax(output, axis=1)


        auc = roc_auc_score(labels, score)

        pos_tot = np.sum(labels).astype(np.float)
        neg_tot = len(labels) - pos_tot

        tp = np.sum(np.logical_and(predicts == 1, labels == 1))
        tn = np.sum(np.logical_and(predicts == 0, labels == 0))
        fn = pos_tot - tp
        fp = neg_tot - tn

        precision = tp / (tp + fp)
        recall = tp / pos_tot
        f_score = 2.*(precision*recall) / (precision+recall)

        mcc = ( tp*tn - fp*fn ) / np.sqrt( (tp+fp)*(tp+fn)*(tn+fp)*(tn+fn) )
        # return err, loss, auc, (tp, tn), f_score, mcc
        return {'auc':auc, 'f_score':f_score, 'tp':tp/pos_tot, 'tn':tn/neg_tot, 'mcc': mcc}

    if load_path:
        load_path = os.path.join(load_path,
            "[{0:0%d}].npz" % (math.floor(math.log(num_epochs, 10))+1))
    #
    # ds = OrderedDict([
    #     ('validation', {
    #         'dataset': (X_val, y_val),
    #         "criterions": [
    #             ('auc', 'largest', 'all'),
    #             ('mcc', 'largest', 'all'),
    #             ('f_score', 'largest', 'all')
    #         ]
    #     })
    #
    #
    ds = OrderedDict()

    for k,d in test_sets:
        ds[k] = {
            "dataset": (d.windows, d.labels),
            "criterions": []
        }

    monitor = Monitor(
        epochs = epochs,
        datasets = ds,
        funcs = [val_epoch],
        h5fp = fp
    )

    # We iterate over epochs:
    for epoch in epochs:

        start_time = time.time()

        filename = load_path.format(epoch)

        params = np.load(filename)
        params = {k:v for k,v in params.items()}
        # print params
        if zero_bias:
            for k,v in params.items():
                if len(v.shape) == 1:
                    params[k][:] = 0.0

        key = [int(x) for x in params.keys()]
        key.sort()

        params = [params[str(k)] for k in key]
        # print params

        lasagne.layers.set_all_param_values(model, params)

        epoch_duration = time.time() - start_time
        monitor.update(epoch_duration)
