
from __future__ import print_function
import argparse
import h5py
import os, sys, shutil
import numpy

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import pandas as pd
from pandas.plotting import table

def get_config(configpath):
    config = {}
    with open(configpath,'r') as f:
        for line in f:
            seg = [x.strip() for x in line.split('=')]
            try:
                a = eval(seg[1])
            except:
                a = seg[1]
            finally:
                config[seg[0]] = a
    return config

def select_candidate(data, diff=0.01, tp=0.8, tn=0.8):
    d = {k:data[k][:] for k in data.keys()}
    flag = numpy.ones(d['tp'].shape)
    flag *= (abs(d['tp']-d['tn']) < diff)
    flag *= (d['tp'] > tp)
    flag *= (d['tn'] > tn)
    flag = flag.nonzero()[0]
    return flag


def visualize_dataset(fileTraining, config, diff=0.01, tp=0.8, tn=0.8, figname=None):

    dirname = fileTraining.split('/')[-2]
    ds = config['dataset'][0]
    figname = figname or dirname + ".pdf"

    with h5py.File(fileTraining, 'r') as fp:
        data = {k:fp['test'][k].value for k in fp['test'].keys()}

        da = numpy.abs(data['tp'] - data['tn'])
        idx = da.argsort()[:max(50, len(da)//20)]

        idx = select_candidate(data, diff=diff, tp=tp, tn=tn)
        tp_sel = data['tp'][idx]
        tn_sel = data['tn'][idx]

        fig = plt.figure(figsize=(8.27,11.69))

        fig.suptitle('Analysis of %s in %s' % (ds, dirname),
            fontsize=14, fontweight='bold')

        ax1 = fig.add_subplot(311)



        ax1.scatter(data['tp'],data['tn'], color='red', s=1)
        ax1.set_xlim([0,1]);ax1.set_ylim([0,1]);plt.grid()
        ax1.plot([0,1],[0,1], '--', color="#333333", )
        ax1.set_xlabel('TP'); ax1.set_ylabel('TN')
        ax1.set_title('TP/TN Distribution across Epochs')
        ax1.grid()


        if len(idx):

            ax2 = fig.add_subplot(312)
            ax3 = fig.add_subplot(313)


            sortIdx = numpy.argsort(data['auc'][idx])[::-1]
            idx = idx[sortIdx[:10]]
            d = {k:data[k][idx] for k in data.keys()}
            d.update({'Epoch':idx+1})

            df = pd.DataFrame(d)


            # ax2.scatter(tp_sel,tn_sel, color='red', s=3)
            ax2.scatter(d['tp'],d['tn'], color='red', s=3)
            for i, e in enumerate(idx):
                ax2.annotate(" %d" % (e+1), (d['tp'][i],d['tn'][i]))
            ax2.plot([0,1],[0,1], '--', color="#333333", )
            ax2.set_xlabel('TP'); ax2.set_ylabel('TN')
            xy_min = numpy.round(numpy.min([numpy.min(tp_sel),numpy.min(tn_sel)]),3)
            xy_max = numpy.round(numpy.max([numpy.max(tp_sel),numpy.max(tn_sel)]),3)
            xy_gap = (xy_max-xy_min)/2
            ax2.set_xlim([xy_min-xy_gap, xy_max+xy_gap])
            ax2.set_ylim([xy_min-xy_gap, xy_max+xy_gap])
            ax2.grid()
            ax2.set_title('TP/TN Distribution across Selected Epochs')

            ax3.xaxis.set_visible(False)  # hide the x axis
            ax3.yaxis.set_visible(False)  # hide the y axis
            ax3.set_frame_on(False)  # no visible frame, uncomment if size is ok
            tabla = table(ax3, df, loc='upper right', colWidths=[0.17]*len(df.columns))  # where df is your data frame
            tabla.auto_set_font_size(False) # Activate set fontsize manually
            tabla.set_fontsize(6) # if ++fontsize is necessary ++colWidths

        else:
            print("Failed to select data points. Please change the criterions.")
            # tabla.scale(1.2, 1.2) # change size table

        plt.tight_layout()
        fig.subplots_adjust(top=0.9)
        ax1.grid()
        plt.savefig(figname, dpi=300)

parser = argparse.ArgumentParser()

parser.add_argument('--tp', default=0.5, type=float,
                    help='Lower bound of TP')
parser.add_argument('--tn', default=0.5, type=float,
                    help='Lower bound of TN')
parser.add_argument('--diff', default=0.01, type=float,
                    help='Upper bound of the differece between TP and TN')
parser.add_argument('dirpath', default='', type=str,
                    help='directoy to the training record and the model parameters.')
parser.add_argument('--filename', default=None, type=str,
                    help='Name of the output file.')

args = parser.parse_args()

if __name__ == "__main__":

    fileTraining = os.path.join( args.dirpath, 'training_records.h5')
    filepath = os.path.join(args.dirpath, 'training_records.pdf')
    config = get_config( os.path.join(args.dirpath, 'config' ))
    visualize_dataset(fileTraining, config, figname=filepath,
        tp = args.tp, tn = args.tn, diff = args.diff)
