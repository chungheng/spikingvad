import argparse
import numpy

import theano
from theano import tensor as T

from vad_mlp import MLP, train_mlp
from vad_utils import shared_dataset, load_cleanspeech_data

parser = argparse.ArgumentParser()

parser.add_argument('--learning_rate', default=0.01, type=float,
                    help='Initial learning rate.')
parser.add_argument('--max_epoch', default=2000, type=int,
                    help='Number of epochs.')
parser.add_argument('--hidden1', default=128, type=int,
                    help='Number of units in hidden layer 1.')
parser.add_argument('--hidden2', default=32, type=int,
                    help='Number of units in hidden layer 2.')
parser.add_argument('--batch_size', default=200, type=int,
                    help='Batch size.  Must divide evenly into the dataset sizes.')
parser.add_argument('--train_dir', default='data', type=str,
                    help='Directory to put the training data.')
parser.add_argument('--fake_data', default=False, type=bool,
                    help='If true, uses fake data for unit testing.')
parser.add_argument('--context_window', default='-2, -1, 0, 1, 2', type=str,
                    help='Context window, a string representing a list of integers speparated by commas.')
parser.add_argument('--prefix', default='', type=str,
                    help='Prefix to the dataset.')

args = parser.parse_args()

def test_mlp_vad(learning_rate=0.01, L1_reg=0.00, L2_reg=0.0001, n_epochs=1000,
             batch_size=20, nbit=2, n_hidden=500, n_hiddenLayers=3, m = 1,
             verbose=False):

    # load datasets
    train_set = load_cleanspeech_data('training')
    valid_set = load_cleanspeech_data('evaluation')
    test_set = load_cleanspeech_data('testing')

    # Convert raw dataset to Theano shared variables.
    train_set_x, train_set_y = shared_dataset(train_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    test_set_x, test_set_y = shared_dataset(test_set)

    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    rng = numpy.random.RandomState(1234)

    # construct a neural network
    classifier = MLP(
        rng=rng,
        input=x,
        n_in=nbit,
        n_hidden=n_hidden,
        n_hiddenLayers=n_hiddenLayers,
        n_out=2
    )

    # the cost we minimize during training is the negative log likelihood of
    # the model plus the regularization terms (L1 and L2); cost is expressed
    # here symbolically
    cost = (
        classifier.negative_log_likelihood(y)
        + L1_reg * classifier.L1
        + L2_reg * classifier.L2_sqr
    )

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    test_model = theano.function(
        inputs=[index],
        outputs=classifier.errors(y),
        givens={
            x: test_set_x[index * batch_size:(index + 1) * batch_size],
            y: test_set_y[index * batch_size:(index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        inputs=[index],
        outputs=classifier.errors(y),
        givens={
            x: valid_set_x[index * batch_size:(index + 1) * batch_size],
            y: valid_set_y[index * batch_size:(index + 1) * batch_size]
        }
    )

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = [T.grad(cost, param) for param in classifier.params]

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs

    # given two lists of the same length, A = [a1, a2, a3, a4] and
    # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
    # element is a pair formed from the two lists :
    #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
    updates = [
        (param, param - learning_rate * gparam)
        for param, gparam in zip(classifier.params, gparams)
    ]

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    err, rt = train_mlp(train_model, validate_model, test_model,
        n_train_batches, n_valid_batches, n_test_batches, n_epochs, verbose)

    return err, rt

if __name__ == "__main__":
    test_mlp_vad()
